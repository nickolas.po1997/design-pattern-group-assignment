package com.example.Assignment.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/")
public class SampleController {

    @RequestMapping(method = RequestMethod.GET)
    public String index(){
        return "HIHI";
    }

}
